package svatek.panels;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;

/**
 * 
 * @author Svatek
 *
 *         Panel kombinující v sobě MenuPanel a TablePanel. Společně tvoří
 *         základ pro gui ovládání aplikace.
 *
 */
public class DataSetPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the panel.
	 */
	public DataSetPanel() {
		init();
	}

	/**
	 * Metoda inicializuje všechny komponenty. Panel využívá gridBagLayout pro
	 * skládání komponent.
	 */
	private void init() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 766, 10, 0 };
		gridBagLayout.rowHeights = new int[] { 211, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);
		setPreferredSize(new Dimension(986, 507));

		TablePanel tablePanel = new TablePanel();
		GridBagConstraints gbc_tablePanel = new GridBagConstraints();
		gbc_tablePanel.fill = GridBagConstraints.BOTH;
		gbc_tablePanel.insets = new Insets(0, 0, 0, 5);
		gbc_tablePanel.gridx = 0;
		gbc_tablePanel.gridy = 0;
		add(tablePanel, gbc_tablePanel);

		MenuPanel menuPanel = new MenuPanel();
		// Nastavení menuPanelu tablePanel, aby mohlo Menu měnit zobrazené údaje v
		// tabulce tablePanelu
		menuPanel.setTablePanel(tablePanel);
		GridBagConstraints gbc_menuPanel = new GridBagConstraints();
		gbc_menuPanel.fill = GridBagConstraints.VERTICAL;
		gbc_menuPanel.gridx = 1;
		gbc_menuPanel.gridy = 0;
		gbc_menuPanel.weightx = 0.2;
		add(menuPanel, gbc_menuPanel);
	}

}
