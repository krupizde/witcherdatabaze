package svatek.panels;

import javax.swing.JPanel;

import svatek.main.JumpForm;
import svatek.main.PrisadyForm;
import svatek.main.PrisadyViewForm;
import svatek.persistence.Database;
import svatek.persistence.dao.AbstractAlchemisticke;
import svatek.persistence.dao.AbstractDao;
import svatek.persistence.dao.ElixirDao;
import svatek.persistence.dao.KategorieDao;
import svatek.persistence.dao.MonstrumDao;
import svatek.persistence.dao.OlejDao;
import svatek.persistence.dao.PrisadaDao;
import svatek.persistence.dao.ZnameniDao;

import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

/**
 * 
 * @author Svatek
 * 
 *         Panel reprezentující menu pro ovládání aplikace. Panel obsahuje
 *         tlačítka pro ovládání akcí a radioButtony pro zvolení tabulky se
 *         kterou se bude pracovat.
 *
 */
public class MenuPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the panel.
	 */
	public MenuPanel() {
		init();

	}

	private TablePanel tablePanel;
	private JRadioButton rdbtnKategorie;
	private JRadioButton rdbtnMonstrum;
	private JRadioButton rdbtnZnameni;
	private JRadioButton rdbtnOleje;
	private JRadioButton rdbtnElixiry;
	private JRadioButton rdbtnPrisady;
	private MyButtonGroup bg;
	private AbstractDao<?> dao;
	private JButton btnPridejPrisadu;
	private JButton btnPrisady;
	private JButton button;
	private JButton btnOdeberPrisadu;

	public void setTablePanel(TablePanel panel) {
		tablePanel = panel;
	}

	private void init() {
		MenuPanel self = this;
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 151, 0 };
		gridBagLayout.rowHeights = new int[] { 45, 45, 45, 45, 45, 45, 45, 0, 0, 0, 0, 0, 0, 50, 45, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);
		this.setMinimumSize(new Dimension(150, 180));

		JButton btnSelect = new JButton("SELECT");

		// Akce po kliknutí na tlačítko select
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// pokud není nastaven zdroj dat (Ještě se neklilo na žádný radioButton), tak
				// nedělej nic
				if (dao == null)
					return;

				// ze zdroje dat se vyberou všechny záznamy, pomocí EntityTableTransformeru se
				// převedou na TableModel a
				// nastaví se tabulce z tablePanel jako model.
				tablePanel.setModel(EntityTableTransformer.getInstance().transform(dao.getAll()));
			}
		});
		btnSelect.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_btnSelect = new GridBagConstraints();
		gbc_btnSelect.fill = GridBagConstraints.BOTH;
		gbc_btnSelect.insets = new Insets(0, 0, 5, 0);
		gbc_btnSelect.gridx = 0;
		gbc_btnSelect.gridy = 0;
		add(btnSelect, gbc_btnSelect);

		JButton btnDelete = new JButton("DELETE");

		// Akce po kliknutí na tlačítko delete
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// z tablePanel se vybere tabulka
				JTable table = tablePanel.getTable();

				// pokud není nastaven zdroj dat (Ještě se neklilo na žádný radioButton) nebo
				// není kliknuto na žádný řádek, tak nedělej nic
				if (dao == null || table.getSelectedRow() == -1)
					return;

				// z tabulky se vybere id řádku, na který je kliknuto (nultý sloupec je vždy id)
				dao.deleteById(Integer.parseInt(table.getModel().getValueAt(table.getSelectedRow(), 0).toString()));

				// obsah tabulky se aktualizuje
				tablePanel.setModel(EntityTableTransformer.getInstance().transform(dao.getAll()));
			}
		});
		btnDelete.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_btnDelete = new GridBagConstraints();
		gbc_btnDelete.insets = new Insets(0, 0, 5, 0);
		gbc_btnDelete.fill = GridBagConstraints.BOTH;
		gbc_btnDelete.gridx = 0;
		gbc_btnDelete.gridy = 1;
		add(btnDelete, gbc_btnDelete);

		JButton btnUpdate = new JButton("UPDATE");

		// Akce po kliknutí na tlačítko update
		btnUpdate.addActionListener(new ActionListener() {
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public void actionPerformed(ActionEvent e) {

				// z tablePanel se vybere tabulka
				JTable table = tablePanel.getTable();

				// pokud není nastaven zdroj dat (Ještě se neklilo na žádný radioButton) nebo
				// není kliknuto na žádný řádek, tak nedělej nic
				if (dao == null || table.getSelectedRow() == -1)
					return;

				// z tabulky se vybere id řádku, na který je kliknuto (nultý sloupec je vždy id)
				int id = Integer.parseInt(table.getModel().getValueAt(table.getSelectedRow(), 0).toString());

				// Vytvoř a otevři nový jumpForm, který slouží pro update/insert záznamů do
				// tabulek
				JumpForm form = new JumpForm(dao, dao.getById(id));
				form.setVisible(true);
			}
		});
		btnUpdate.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_btnUpdate = new GridBagConstraints();
		gbc_btnUpdate.insets = new Insets(0, 0, 5, 0);
		gbc_btnUpdate.fill = GridBagConstraints.BOTH;
		gbc_btnUpdate.gridx = 0;
		gbc_btnUpdate.gridy = 2;
		add(btnUpdate, gbc_btnUpdate);

		JButton btnInsert = new JButton("INSERT");

		// Akce po kliknutí na tlačítko insert
		btnInsert.addActionListener(new ActionListener() {
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public void actionPerformed(ActionEvent e) {

				// pokud není nastaven zdroj dat (Ještě se neklilo na žádný radioButton) tak
				// nedělej nic
				if (dao == null)
					return;

				// Vytvoř a otevři nový jumpForm, který slouží pro update/insert záznamů do
				// tabulek
				JumpForm form = new JumpForm(dao);
				form.setVisible(true);
			}
		});
		btnInsert.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_btnInsert = new GridBagConstraints();
		gbc_btnInsert.insets = new Insets(0, 0, 5, 0);
		gbc_btnInsert.fill = GridBagConstraints.BOTH;
		gbc_btnInsert.gridx = 0;
		gbc_btnInsert.gridy = 3;
		add(btnInsert, gbc_btnInsert);

		btnPridejPrisadu = new JButton("PRIDEJ PRISADU");

		// Akce po kliknutí na tlačítko PridejPrisadu
		btnPridejPrisadu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// z tablePanel vyber tabulku
				JTable table = tablePanel.getTable();

				// pokud není kliknuto na žádný řádek, tak nedělej nic (že je vybrán zdroj dat
				// je zaručeno)
				if (table.getSelectedRow() == -1)
					return;

				// z tabulky vyber id řádku, na který je kliknuto (nultý sloupec je vždy id)
				int id = Integer.parseInt(table.getModel().getValueAt(table.getSelectedRow(), 0).toString());

				// Vytvoř a otevři nový PrisadyPridejFrom, který slouží pro přidávání jedné
				// přísady najednou. Je poslán příznak false, aby se form spustil pro přidávání
				// a ne pro odebírání
				PrisadyForm form = new PrisadyForm(id, dao, false);
				form.setVisible(true);
			}
		});
		btnPridejPrisadu.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_btnPridejPrisadu = new GridBagConstraints();
		gbc_btnPridejPrisadu.fill = GridBagConstraints.BOTH;
		gbc_btnPridejPrisadu.insets = new Insets(0, 0, 5, 0);
		gbc_btnPridejPrisadu.gridx = 0;
		gbc_btnPridejPrisadu.gridy = 4;
		add(btnPridejPrisadu, gbc_btnPridejPrisadu);

		btnOdeberPrisadu = new JButton("ODEBER PRISADU");

		// Akce po kliknutí na tlačítko OdeberPrisadu
		btnOdeberPrisadu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// z tablePanel vyber tabulku
				JTable table = tablePanel.getTable();

				// pokud není kliknuto na žádný řádek, tak nedělej nic (že je vybrán zdroj dat
				// je zaručeno)
				if (table.getSelectedRow() == -1)
					return;

				// z tabulky vyber id řádku, na který je kliknuto (nultý sloupec je vždy id)
				int id = Integer.parseInt(table.getModel().getValueAt(table.getSelectedRow(), 0).toString());

				// Vytvoř a otevři nový PrisadyPridejFrom, který slouží pro přidávání jedné
				// přísady najednou. Je poslán příznak true, aby se form otevřel v módu pro
				// odebírání a ne přidávání
				PrisadyForm form = new PrisadyForm(id, dao, true);
				form.setVisible(true);
			}
		});
		btnOdeberPrisadu.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_btnOdeberPrisadu = new GridBagConstraints();
		gbc_btnOdeberPrisadu.fill = GridBagConstraints.BOTH;
		gbc_btnOdeberPrisadu.insets = new Insets(0, 0, 5, 0);
		gbc_btnOdeberPrisadu.gridx = 0;
		gbc_btnOdeberPrisadu.gridy = 5;
		add(btnOdeberPrisadu, gbc_btnOdeberPrisadu);

		btnPrisady = new JButton("ZOBRAZ PRISADY");

		// Akce po kliknutí na tlačítko zobrazPrisady
		btnPrisady.addActionListener(new ActionListener() {
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public void actionPerformed(ActionEvent e) {
				// z tablePanel vyber tabulku
				JTable table = tablePanel.getTable();

				// pokud není kliknuto na žádný řádek, tak nedělej nic (že je vybrán zdroj dat
				// je zaručeno)
				if (table.getSelectedRow() == -1)
					return;

				// z tabulky vyber id řádku, na který je kliknuto (nultý sloupec je vždy id)
				int id = Integer.parseInt(table.getModel().getValueAt(table.getSelectedRow(), 0).toString());

				// Vytvoří a otevře nový prisadyViewForm, který přijímá jako parametr kolkci
				// přísad, které se vyberou pomocí dao a vybraného id
				PrisadyViewForm form = new PrisadyViewForm(((AbstractAlchemisticke) dao).getPrisady(id));
				form.setVisible(true);
			}
		});
		btnPrisady.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_btnPrisady = new GridBagConstraints();
		gbc_btnPrisady.insets = new Insets(0, 0, 5, 0);
		gbc_btnPrisady.fill = GridBagConstraints.BOTH;
		gbc_btnPrisady.gridx = 0;
		gbc_btnPrisady.gridy = 6;
		add(btnPrisady, gbc_btnPrisady);

		// RadioButtony pro výběr zdroje dat

		rdbtnKategorie = new JRadioButton("Kategorie");
		rdbtnKategorie.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_rdbtnKategorie = new GridBagConstraints();
		gbc_rdbtnKategorie.insets = new Insets(0, 0, 5, 0);
		gbc_rdbtnKategorie.gridx = 0;
		gbc_rdbtnKategorie.gridy = 7;
		add(rdbtnKategorie, gbc_rdbtnKategorie);

		rdbtnMonstrum = new JRadioButton("Monstra");
		rdbtnMonstrum.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_rdbtnMonstrum = new GridBagConstraints();
		gbc_rdbtnMonstrum.insets = new Insets(0, 0, 5, 0);
		gbc_rdbtnMonstrum.gridx = 0;
		gbc_rdbtnMonstrum.gridy = 8;
		add(rdbtnMonstrum, gbc_rdbtnMonstrum);

		rdbtnZnameni = new JRadioButton("Znameni");
		rdbtnZnameni.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_rdbtnZnameni = new GridBagConstraints();
		gbc_rdbtnZnameni.insets = new Insets(0, 0, 5, 0);
		gbc_rdbtnZnameni.gridx = 0;
		gbc_rdbtnZnameni.gridy = 9;
		add(rdbtnZnameni, gbc_rdbtnZnameni);

		rdbtnOleje = new JRadioButton("Oleje");
		rdbtnOleje.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_rdbtnOleje = new GridBagConstraints();
		gbc_rdbtnOleje.insets = new Insets(0, 0, 5, 0);
		gbc_rdbtnOleje.gridx = 0;
		gbc_rdbtnOleje.gridy = 10;
		add(rdbtnOleje, gbc_rdbtnOleje);

		rdbtnElixiry = new JRadioButton("Elixiry");
		rdbtnElixiry.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_rdbtnElixiry = new GridBagConstraints();
		gbc_rdbtnElixiry.insets = new Insets(0, 0, 5, 0);
		gbc_rdbtnElixiry.gridx = 0;
		gbc_rdbtnElixiry.gridy = 11;
		add(rdbtnElixiry, gbc_rdbtnElixiry);

		rdbtnPrisady = new JRadioButton("Prisady");
		rdbtnPrisady.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_rdbtnPrisady = new GridBagConstraints();
		gbc_rdbtnPrisady.insets = new Insets(0, 0, 5, 0);
		gbc_rdbtnPrisady.gridx = 0;
		gbc_rdbtnPrisady.gridy = 12;
		add(rdbtnPrisady, gbc_rdbtnPrisady);

		// RadioButtony jsou přídány do MyButtonGroup, aby mohl být vybrán pouze jeden
		// zároveň.
		bg = new MyButtonGroup();
		bg.add(rdbtnPrisady);
		bg.add(rdbtnElixiry);
		bg.add(rdbtnOleje);
		bg.add(rdbtnZnameni);
		bg.add(rdbtnMonstrum);
		bg.add(rdbtnKategorie);

		button = new JButton("LOGOUT");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(self);
				frame.getContentPane().removeAll();
				Database.getInstance().close();
				frame.repaint();
				frame.getContentPane().add(new LoginPanel());
				SwingUtilities.updateComponentTreeUI(frame);
			}
		});
		button.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.fill = GridBagConstraints.BOTH;
		gbc_button.gridx = 0;
		gbc_button.gridy = 14;
		add(button, gbc_button);

		// Listener pro kliknutí na nějaký RadioButton z buttonGroup. Po kliknutí na
		// nějaký z nich se dao nastaví na daný zdroj dat a tabulka se přenačte. Taky
		// podle druhu radioButtonu jsou viditelná tlačíka PridajPrisadu a ZobrazPrisady
		bg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (bg.getSelection().equals(rdbtnPrisady.getModel())) {
					dao = new PrisadaDao();
					schovejTlacitka();
				} else if (bg.getSelection().equals(rdbtnElixiry.getModel())) {
					dao = new ElixirDao();
					ukazTlacitka();
				} else if (bg.getSelection().equals(rdbtnOleje.getModel())) {
					dao = new OlejDao();
					ukazTlacitka();
				} else if (bg.getSelection().equals(rdbtnZnameni.getModel())) {
					dao = new ZnameniDao();
					schovejTlacitka();
				} else if (bg.getSelection().equals(rdbtnMonstrum.getModel())) {
					dao = new MonstrumDao();
					schovejTlacitka();
				} else if (bg.getSelection().equals(rdbtnKategorie.getModel())) {
					dao = new KategorieDao();
					schovejTlacitka();
				} else {
					return;
				}
				tablePanel.setModel(EntityTableTransformer.getInstance().transform(dao.getAll()));

			}
		});
		schovejTlacitka();
	}

	/**
	 * Metoda schová tlačítka PridejPrisady a ZobrazPrisady
	 */
	private void schovejTlacitka() {
		btnPrisady.setVisible(false);
		btnPridejPrisadu.setVisible(false);
		btnOdeberPrisadu.setVisible(false);
	}

	/**
	 * Metoda zobrazí tlačítka PridejPrisady a ZobrazPrisady
	 */
	private void ukazTlacitka() {
		btnPrisady.setVisible(true);
		btnPridejPrisadu.setVisible(true);
		btnOdeberPrisadu.setVisible(true);
	}

}
