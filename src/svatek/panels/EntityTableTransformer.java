package svatek.panels;

import java.lang.reflect.Field;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import svatek.entity.Entita;

/**
 * 
 * @author Svatek
 * 
 *         Metoda slouží jako transformátor objektů do tabulkového modelu, který
 *         se pak dá nastavit objektům typu JTable. Vpodstatě projdě všechny
 *         proměnné ve Třídovém objektu dané třídy (Objekt uchovávající všechny
 *         informace o dané třídě, který se Java vytvoří sama při kompilaci) a
 *         nastaví je jako tabulkový header. Následně se zavolá metoda getLine,
 *         která v sobě uchovává hodnoty všech proměnných daného objektu jako
 *         Stringovské pole. Třída je singleton.
 *
 */
public class EntityTableTransformer {

	private EntityTableTransformer() {
	}

	private static EntityTableTransformer instance;

	/**
	 * Singleton metoda
	 * 
	 * @return Jedinou instanci EntityTableTransformer
	 */
	public static EntityTableTransformer getInstance() {
		if (instance == null)
			instance = new EntityTableTransformer();
		return instance;
	}

	/**
	 * Metoda transformující list objektů na tabulkový model. Metoda je citlivá na
	 * pořadí deklarace proměnných ve Třídě T (Tedy pořadí deklarace proměnných v
	 * entitě). Třída využívá generika.
	 * 
	 * @param entity List generického datového typu T, o kterém víme z deklarace (<T
	 *               extends Entita>), že dědí od třídy Entita
	 * @return DefaultTableModel obsahující data z entity
	 */
	public <T extends Entita> DefaultTableModel transform(List<T> entity) {
		DefaultTableModel model = new DefaultTableModel() {

			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int row, int column) {
				return false;
			};
		};
		if (entity.isEmpty())
			return model;

		model.addColumn("id");
		Field[] fields = entity.get(0).getClass().getDeclaredFields();
		for (Field f : fields) {
			model.addColumn(f.getName());
		}
		for (T entita : entity) {
			model.addRow(entita.getLine());
		}
		return model;
	}
}
