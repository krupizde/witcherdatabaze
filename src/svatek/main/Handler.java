package svatek.main;

import javax.swing.JOptionPane;

import svatek.persistence.Database;

/**
 * 
 * @author Svatek, https://www.baeldung.com/java-global-exception-handler
 * 
 *         Třída, která se stará o zvládnutí všech neodchycených Vyjímek, které
 *         jsou obvykle vyhozeny pomocí "throw new RuntimeException(text)".
 *         Pokusí se zalogovat vyjímku do databáze a její zprávu a otevře
 *         Message box s touto zprávou
 * 
 *
 */
public class Handler implements Thread.UncaughtExceptionHandler {

	public void uncaughtException(Thread t, Throwable e) {
		if (e.getMessage().contains("SELECT") && e.getMessage().contains("denied"))
			return;
		Database.getInstance().addClientLog(e.getClass().getCanonicalName(), e.getMessage());
		JOptionPane.showMessageDialog(null, t.getName() + "\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
	}
}
