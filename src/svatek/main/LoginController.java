package svatek.main;

import svatek.persistence.Database;

/**
 * 
 * @author Svatek
 *
 *         Singleton třída sloužící pro přihlášení uživatele. Jediné co třída
 *         udělá je, že se pokusí vytvořit připojení k databázi podle zadaných
 *         údajů. Pokud to vyjde, vrací true, pokud ne, vrací false
 */
public class LoginController {

	private LoginController() {
	}

	private static LoginController instance;

	public static LoginController getInstance() {
		if (instance == null)
			instance = new LoginController();
		return instance;
	}

	public boolean login(String username, String password) {
		try {
			Database.getInstance().login(username, password);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
