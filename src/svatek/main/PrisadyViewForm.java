package svatek.main;

import java.awt.BorderLayout;
import java.util.List;

import javax.swing.JFrame;

import svatek.entity.Prisada;
import svatek.panels.EntityTableTransformer;
import svatek.panels.TablePanel;

/**
 * 
 * @author Svatek
 * 
 *         Formulář, který pouze zobrazuje list přísad v tabulce.
 *
 */
public class PrisadyViewForm extends JFrame {

	private static final long serialVersionUID = 1L;

	public PrisadyViewForm(List<Prisada> prisady) {
		super();
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setSize(500, 350);

		getContentPane().setLayout(new BorderLayout(0, 0));

		TablePanel panel = new TablePanel();
		getContentPane().add(panel);

		panel.setModel(EntityTableTransformer.getInstance().transform(prisady));
	}
}
