package svatek.main;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import svatek.entity.Prisada;
import svatek.panels.EntityTableTransformer;
import svatek.panels.TablePanel;
import svatek.persistence.dao.AbstractAlchemisticke;
import svatek.persistence.dao.AbstractDao;
import svatek.persistence.dao.PrisadaDao;

/**
 * 
 * @author Svatek
 * 
 *         Formulář, který zobrazí tabulku přísad, v závislosti na příznaku
 *         odebirani, buď které k danému Oleji/Elixíru patří nebo naopak
 *         nepatří. Po dvojkliku na nějaký řádek této tabulky se daná přísada
 *         přidá/odebere k/od daného Oleje/Elixíru
 *
 */
public class PrisadyForm extends JFrame {

	private static final long serialVersionUID = 1L;

	public PrisadyForm(int id, AbstractDao<?> dao, boolean odebirani) {
		AbstractAlchemisticke<?> newDao = (AbstractAlchemisticke<?>) dao;
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setSize(500, 350);

		getContentPane().setLayout(new BorderLayout(0, 0));

		TablePanel panel = new TablePanel();
		getContentPane().add(panel);

		List<Prisada> prisady = new ArrayList<Prisada>();
		if (odebirani) {
			prisady = newDao.getPrisady(id);
		} else {
			List<Prisada> prisadyOld = new PrisadaDao().getAll();
			List<Prisada> prisadyPripravku = newDao.getPrisady(id);

			// Vyfiltruje přísady, které jsou již k Oleji/Elixíru přiřazeny
			for (Prisada p : prisadyOld) {
				if (!prisadyPripravku.contains(p)) {
					prisady.add(p);
				}
			}
		}

		// Dané přísady se přes EntityTableTransformer nastaví jako model pro tabulku
		panel.setModel(EntityTableTransformer.getInstance().transform(prisady));

		JFrame self = this;
		// Přidá naslouchač pro kliknutí myší na tabulku
		panel.getTable().addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {

				// Při dvojkliku na nějaký řádek
				if (e.getClickCount() == 2 && panel.getTable().getSelectedRow() != -1) {

					// Se získá id přísady na daném řádku
					int idPrisada = Integer.parseInt(
							panel.getTable().getModel().getValueAt(panel.getTable().getSelectedRow(), 0).toString());
					// Dle příznaku se určí, zda se má přidávat nebo odebírat přísada
					if (odebirani) {
						newDao.removePrisada(id, idPrisada);
					} else {
						// přiřadí se k Oleji/Elixíru daná přísada podle id
						newDao.addPrisada(id, idPrisada);

					}

					// toto okno se zavře
					self.dispose();
				}

			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}
		});
	}
}
