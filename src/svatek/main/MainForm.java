package svatek.main;

import java.awt.EventQueue;

import javax.swing.JFrame;

import svatek.panels.LoginPanel;
import svatek.persistence.Database;

import java.awt.BorderLayout;

/**
 * 
 * @author Svatek, https://www.geeksforgeeks.org/jvm-shutdown-hook-java/
 *
 *         Hlavní formulář aplikace, ve kterém se nachází metoda main. Obsahem
 *         tohoto formuláře je defaultně loginPanel pro přihlášení uživatele.
 *         Nastavuje se zde také vlákno, které se spustí vždy při ukončení
 *         aplikace a nastavení defaultního Handleru pro neodchycené vyjímky.
 *
 */
public class MainForm {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Handler globalExceptionHandler = new Handler();
		Thread.setDefaultUncaughtExceptionHandler(globalExceptionHandler);
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			public void run() {
				Database.getInstance().close();
			}
		}, "Shutdown-thread"));
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm window = new MainForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainForm() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1100, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));

		LoginPanel loginPanel = new LoginPanel();
		frame.getContentPane().add(loginPanel);
	}

}
