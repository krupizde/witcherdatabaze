package svatek.main;

/**
 * 
 * @author Svatek
 * 
 *         Enum pro názvy tabulek v databázi
 *
 */
public enum TablesEnum {

	ELIXIR, KATEGORIE, MONSTRUM, OLEJ, PRISADA, ZNAMENI;
}
