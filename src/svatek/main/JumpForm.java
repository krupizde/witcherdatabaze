package svatek.main;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import svatek.entity.Entita;
import svatek.entity.Monstrum;
import svatek.persistence.dao.AbstractDao;

/**
 * 
 * @author Svatek,
 *         https://stackoverflow.com/questions/2126714/java-get-all-variable-names-in-a-class,
 *         https://docs.oracle.com/javase/tutorial/reflect/class/classMembers.html
 *
 *         Formulář sloužící pro aktualizaci/vkládání dat do databáze. Obsah
 *         tohoto formuláře se generuje na základě zdroje dat, dao, a
 *         generického datového typu T. Při vytváření formuláře je pomocí
 *         dao.getClazz() getnut objekt typu Class pro zadaný generický datový
 *         typ. Poté pomocí getDeclaredFields() je vytaženo pole proměnných z
 *         dané třídy, a na základě jejich typů jsou vygenerovány buď
 *         textFieldy(Integer, String), nebo JRadioButtony(Boolean), s JLabely
 *         jako jejich popisky. AbstractDao<?> značí, že neznáme generický typ,
 *         který bude toto dao obsahovat, a budeme ho vědět až při přiřazení do
 *         této proměnné.
 * 
 * @param <T> Typ entity, se kterou se pracuje
 */
public class JumpForm<T extends Entita> extends JFrame {

	private static final long serialVersionUID = 1L;
	AbstractDao<?> dao;

	/**
	 * Konstruktor pro insert, v init() se do TextFieldů a RadioButtonů nepropisují
	 * žádné hodnoty
	 * 
	 * @param dao Zdroj dat, podle kterého se bude generovat obsah formuláře a na
	 *            který se bude volat save()
	 */
	public JumpForm(AbstractDao<T> dao) {
		super();
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setSize(500, 350);
		this.setLayout(new GridLayout(6, 1, 20, 20));
		this.dao = dao;
		init(dao, null);
	}

	/**
	 * Konstruktor pro update, v init() se do TextFieldů a RadioButtonů propisují
	 * hodnoty z promenné entita
	 * 
	 * @param dao    Zdroj dat, podle kterého se bude generovat obsah formuláře a na
	 *               který se bude volat save()
	 * @param entita Entita, která bude aktualizována
	 */
	public JumpForm(AbstractDao<T> dao, T entita) {
		super();
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setSize(500, 350);
		this.setLayout(new GridLayout(6, 1, 20, 20));
		this.dao = dao;
		init(dao, entita);
	}

	/**
	 * Metoda která vygeneuje obsah tohoto formuláře dle zadaných parametrů
	 * 
	 * @param dao    Zdroj dat
	 * @param entita Entita, se kterou se případně pracuje
	 */
	private void init(AbstractDao<T> dao, T entita) {

		// List panelů. Každý panel na formuláři odpovídá jedné proměnné ze zadané
		// entity, kromě id, které stejně nelze ani měnit, ani manuálně vkládat
		ArrayList<JPanel> components = new ArrayList<JPanel>();

		// Pole proměnných ze zadané třídy
		Field[] fields = dao.getClazz().getDeclaredFields();
		int i = 1;

		// Pro každou proměnnou vytvoř panel
		for (Field f : fields) {

			JPanel panel = new JPanel();
			panel.setLayout(new GridLayout(1, 2));

			// Pokud se jedná o boolean, přidej na panel pouze JRadioButton
			if (f.getType().getName().toLowerCase().equals("boolean")) {
				JRadioButton button = new JRadioButton(f.getName());

				// Pokud entita není null (tedy jde o update), nastav tomuto JRadioButtonu
				// hodnotu
				if (entita != null)
					button.setSelected(((Monstrum) entita).isBoss());

				// Přidej tento RadioButton na panel
				panel.add(button);
			}

			// Pokud se nejedná o Boolean, jedná se o buďto String, nebo Integer (Alespoň v
			// tomto případě). Vytvoří se tedy JLabel s popisem (Název proměnné) a
			// JTextField pro hodnotu
			else {
				panel.add(new JLabel(f.getName()));
				JTextField text = new JTextField();

				// Pokud entita není null (tedy jde o update), nastav tomuto JTextFieldu hodnotu
				if (entita != null) {
					text.setText(entita.getLine()[i]);
				}
				panel.add(text);
			}

			// Přidej právě vytvořený panel na plochu tohoto formuláře
			this.add(panel);

			// Přidej tento panel také do seznamu komponent
			components.add(panel);
			i++;
		}
		JButton save = new JButton("Uložit");
		save.setSize(200, 100);
		add(save);
		// Akce po kliknutí na tlačítko uložit
		save.addActionListener(new ActionListener() {
			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent e) {

				// Vytvoř instanci typu T. Tento typ je specifikovaný v dao, takže je to v běhu
				// buď Monstrum, Kategorie, Olej atd... Pomocí objektu typu Class lze tedy
				// vytvořit prázdnou instanci typu T, aniž bychom předem věděli, jakého typu je.
				T ent = null;
				try {
					ent = (T) dao.getClazz().newInstance();
				} catch (InstantiationException | IllegalAccessException e2) {
					throw new RuntimeException(e2);
				}

				// Získej seznam proměnných z dané třídy
				Field[] fields = dao.getClazz().getDeclaredFields();

				// Projdi všechny vytvořené panely (odpovícající pořadím proměnným této třídy) a
				// nastav proměnné ent tento parametr podle získané hodnoty z jednotlivých
				// panelů
				for (int i = 0; i < components.size(); i++) {
					try {

						// Nastav, aby dočasně byl tento paramtr dostupný pro set, ikdyž je private
						fields[i].setAccessible(true);

						// Pokud první komponent v procházeném panel je JRadioButton, pak odpovídá
						// proměnné typu boolean
						if (components.get(i).getComponent(0) instanceof JRadioButton) {

							// Získej hodnotu z JRadioButtonu
							boolean selected = ((JRadioButton) components.get(i).getComponent(0)).isSelected();

							// Nastav tuto hodnotu proměnné v objektu ent
							fields[i].set(ent, selected);
						} else {

							// Prošli jsme ifem, zde je tedy pouze jediná možnost a to, že druhý komponent
							// procházeného panelu je zaručeně JTextField. Extrahuji z něj tedy text
							String text = ((JTextField) components.get(i).getComponent(1)).getText();

							// V závislosti na tom, jakého typu je aktuálně procházená proměnná nastavuji
							// buď String nebo int
							if (fields[i].getType().getName().equals(String.class.getName())) {
								fields[i].set(ent, text);
							} else {
								int cis = Integer.parseInt(text);
								fields[i].set(ent, cis);
							}

						}
					} catch (IllegalAccessException | IllegalArgumentException e1) {
						throw new RuntimeException(e1);
					}
				}

				// Pokud vstupní entita není null, tedy se jedná o update, nastavím proměnné ent
				// také id, aby metoda dao.save() poznala, že se jedná o update
				if (entita != null)
					ent.setId(entita.getId());
				dao.save(ent);
			}
		});

	}
}
