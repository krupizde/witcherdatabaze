package svatek.persistence.dao;

import java.util.List;

import svatek.entity.Entita;
import svatek.entity.Prisada;

/**
 * 
 * @author Svatek
 * 
 *         Třída slouží jako rozšíření {@link AbstractDao} pro práci s entitami
 *         spojenými s Přísadami
 *
 * @param <T> Generický typ určující, se kterou třídou bude Dao pracovat
 */
public abstract class AbstractAlchemisticke<T extends Entita> extends AbstractDao<T> {

	/**
	 * Přidá dané entitě přísadu
	 * 
	 * @param id        Id entity
	 * @param idPrisada Id přísady
	 */
	public abstract void addPrisada(int id, int idPrisada);

	/**
	 * Vybere list přísad pro entitu dle zadaného id
	 * 
	 * @param id Id entity
	 * @return List přísad dané entity
	 */
	public abstract List<Prisada> getPrisady(int id);

	/**
	 * Odebere od dané entity danou přísadu
	 * 
	 * @param id        Id entity
	 * @param idPrisada Id přísady
	 */
	public abstract void removePrisada(int id, int idPrisada);

}
