package svatek.persistence.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import svatek.entity.Kategorie;
import svatek.main.TablesEnum;
import svatek.persistence.Database;
import svatek.persistence.QueryParam;

public class KategorieDao extends AbstractDao<Kategorie> {


	@Override
	public List<Kategorie> getAll() {
		ArrayList<Kategorie> tmp;
		try {
			tmp = new ArrayList<>();
			ResultSet set = Database.getInstance().selectResult("select * from kategorie order by id_kategorie", null);
			while (set.next()) {
				tmp.add(new Kategorie(set.getInt(1), set.getInt(2), set.getInt(3), set.getString(4)));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return tmp;
	}

	@Override
	public Kategorie getById(int id) {
		try {
			ResultSet set = Database.getInstance().selectResult("select * from kategorie where id_kategorie = ?",
					new QueryParam[] { new QueryParam<Integer>(id) });
			while (set.next()) {
				return new Kategorie(set.getInt(1), set.getInt(2), set.getInt(3), set.getString(4));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return null;
	}

	@Override
	public void save(Kategorie entita) {
		try {
			if (entita.getId() != 0 && exists(entita)) {
				QueryParam<?>[] params = { new QueryParam<Integer>(entita.getIdOlej()),
						new QueryParam<Integer>(entita.getIdZnameni()), new QueryParam<String>(entita.getNazev()),
						new QueryParam<Integer>(entita.getId()) };
				Database.getInstance().save(
						"update kategorie set id_olej = ?, id_znameni = ?, nazev = ? where id_kategorie = ?", params);
			} else {
				QueryParam<?>[] params = { new QueryParam<Integer>(entita.getIdOlej()),
						new QueryParam<Integer>(entita.getIdZnameni()), new QueryParam<String>(entita.getNazev()) };
				Database.getInstance().save("insert into kategorie(id_olej, id_znameni, nazev) values(?,?,?)", params);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	public void deleteById(int id) {
		try {
			Database.getInstance().delete(TablesEnum.KATEGORIE, id);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}		
	}

	@Override
	public Class<?> getClazz() {
		return Kategorie.class;
	}

}
