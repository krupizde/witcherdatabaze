package svatek.persistence.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import svatek.entity.Prisada;
import svatek.main.TablesEnum;
import svatek.persistence.Database;
import svatek.persistence.QueryParam;

public class PrisadaDao extends AbstractDao<Prisada> {

	@Override
	public List<Prisada> getAll() {
		ArrayList<Prisada> tmp;
		try {
			tmp = new ArrayList<>();
			ResultSet set = Database.getInstance().selectResult("select * from prisada order by id_prisada", null);
			while (set.next()) {
				tmp.add(new Prisada(set.getInt("id_prisada"), set.getString("nazev")));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return tmp;
	}

	@Override
	public Prisada getById(int id) {
		try {
			ResultSet set = Database.getInstance().selectResult("select * from prisada where id_prisada = ?",
					new QueryParam[] { new QueryParam<Integer>(id) });
			while (set.next()) {
				return new Prisada(set.getInt("id_prisada"), set.getString("nazev"));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return null;
	}

	@Override
	public void save(Prisada entita) {
		try {
			if (entita.getId() != 0 && exists(entita)) {
				QueryParam<?>[] params = { new QueryParam<String>(entita.getNazev()),
						new QueryParam<Integer>(entita.getId()) };
				Database.getInstance().save("update prisada set nazev = ? where id_prisada = ?", params);
			} else {
				QueryParam<?>[] params = { new QueryParam<String>(entita.getNazev()) };
				Database.getInstance().save("insert into prisada(nazev) values(?)", params);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	public void deleteById(int id) {
		try {
			Database.getInstance().delete(TablesEnum.PRISADA, id);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public Class<?> getClazz() {
		return Prisada.class;
	}

}
