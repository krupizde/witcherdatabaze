package svatek.persistence.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import svatek.entity.Olej;
import svatek.entity.Prisada;
import svatek.main.TablesEnum;
import svatek.persistence.Database;
import svatek.persistence.QueryParam;

public class OlejDao extends AbstractAlchemisticke<Olej>{

	@Override
	public List<Olej> getAll() {
		ArrayList<Olej> tmp;
		try {
			tmp = new ArrayList<>();
			ResultSet set = Database.getInstance().selectResult("select * from olej order by id_olej", null);
			while (set.next()) {
				tmp.add(new Olej(set.getString("nazev"), set.getInt("id_olej"), set.getString("popis_efektu")));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return tmp;
	}

	@Override
	public Olej getById(int id) {
		try {
			ResultSet set = Database.getInstance().selectResult("select * from olej where id_olej = ?",
					new QueryParam[] { new QueryParam<Integer>(id) });
			while (set.next()) {
				return new Olej(set.getString("nazev"), set.getInt("id_olej"), set.getString("popis_efektu"));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return null;
	}

	@Override
	public void save(Olej entita) {
		try {
			if (entita.getId() != 0 && exists(entita)) {
				QueryParam<?>[] params = { new QueryParam<String>(entita.getNazev()),
						new QueryParam<String>(entita.getPopisEfektu()), new QueryParam<Integer>(entita.getId()) };
				Database.getInstance().save("update olej set nazev = ?, popis_efektu = ? where id_olej = ?", params);
			} else {
				QueryParam<?>[] params = { new QueryParam<String>(entita.getNazev()),
						new QueryParam<String>(entita.getPopisEfektu()) };
				Database.getInstance().save("insert into olej(nazev, popis_efektu) values(?,?)", params);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	public void deleteById(int id) {
		try {
			Database.getInstance().delete(TablesEnum.OLEJ, id);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public Class<?> getClazz() {
		return Olej.class;
	}
	@Override
	public void addPrisada(int id, int idPrisada) {
		try {
			PreparedStatement stm = Database.getInstance().getConnection().prepareStatement("call pridejPrisaduOleji(?,?)");
			stm.setInt(1, id);
			stm.setInt(2, idPrisada);
			stm.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	@Override
	public List<Prisada> getPrisady(int id){
		ArrayList<Prisada> tmp;
		try {
			tmp = new ArrayList<>();
			PreparedStatement stm = Database.getInstance().getConnection().prepareStatement("call getPrisadyOleje(?)");
			stm.setInt(1, id);
			ResultSet set = stm.executeQuery();
			while (set.next()) {
				tmp.add(new Prisada(set.getInt("id_prisada"), set.getString("nazev")));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return tmp;
	}

	@Override
	public void removePrisada(int id, int idPrisada) {
		try {
			PreparedStatement stm = Database.getInstance().getConnection().prepareStatement("call odeberPrisaduOleji(?,?)");
			stm.setInt(1, id);
			stm.setInt(2, idPrisada);
			stm.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}

}
