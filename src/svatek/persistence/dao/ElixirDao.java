package svatek.persistence.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import svatek.entity.Elixir;
import svatek.entity.Prisada;
import svatek.main.TablesEnum;
import svatek.persistence.Database;
import svatek.persistence.QueryParam;

public class ElixirDao extends AbstractAlchemisticke<Elixir> {

	public List<Elixir> getAll() {
		ArrayList<Elixir> tmp;
		try {
			tmp = new ArrayList<>();
			ResultSet set = Database.getInstance().selectResult("select * from elixir order by id_elixir", null);
			while (set.next()) {
				tmp.add(new Elixir(set.getInt(1), set.getString(2), set.getString(3)));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return tmp;
	}

	public Elixir getById(int id) {
		ResultSet set;
		try {
			set = Database.getInstance().selectResult("select * from elixir where id_elixir = ?",
					new QueryParam[] { new QueryParam<Integer>(id) });

			while (set.next()) {
				return new Elixir(set.getInt(1), set.getString(2), set.getString(3));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return null;
	}

	@Override
	public void save(Elixir entita) {
		try {
			if (entita.getId() != 0 && exists(entita)) {
				QueryParam<?>[] params = { new QueryParam<String>(entita.getNazev()),
						new QueryParam<String>(entita.getPopisEfektu()), new QueryParam<Integer>(entita.getId()) };
				Database.getInstance().save("update elixir set nazev = ?, popis_efektu = ? where id_elixir = ?",
						params);
			} else {
				QueryParam<?>[] params = { new QueryParam<String>(entita.getNazev()),
						new QueryParam<String>(entita.getPopisEfektu()) };
				Database.getInstance().save("insert into elixir(nazev, popis_efektu) values(?,?)", params);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	public void deleteById(int id) {
		try {
			Database.getInstance().delete(TablesEnum.ELIXIR, id);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	public Class<?> getClazz() {
		return Elixir.class;
	}

	@Override
	public void addPrisada(int id, int idPrisada) {
		try {
			PreparedStatement stm = Database.getInstance().getConnection()
					.prepareStatement("call pridejPrisaduElixiru(?,?)");
			stm.setInt(1, id);
			stm.setInt(2, idPrisada);
			stm.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Prisada> getPrisady(int id) {
		ArrayList<Prisada> tmp;
		try {
			tmp = new ArrayList<>();
			PreparedStatement stm = Database.getInstance().getConnection()
					.prepareStatement("call getPrisadyElixiru(?)");
			stm.setInt(1, id);
			ResultSet set = stm.executeQuery();
			while (set.next()) {
				tmp.add(new Prisada(set.getInt("id_prisada"), set.getString("nazev")));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return tmp;
	}

	@Override
	public void removePrisada(int id, int idPrisada) {
		try {
			PreparedStatement stm = Database.getInstance().getConnection()
					.prepareStatement("call odeberPrisaduElixiru(?,?)");
			stm.setInt(1, id);
			stm.setInt(2, idPrisada);
			stm.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}
}
