package svatek.persistence.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import svatek.entity.Znameni;
import svatek.main.TablesEnum;
import svatek.persistence.Database;
import svatek.persistence.QueryParam;

public class ZnameniDao extends AbstractDao<Znameni> {

	@Override
	public List<Znameni> getAll() {
		ArrayList<Znameni> tmp;
		try {
			tmp = new ArrayList<>();
			ResultSet set = Database.getInstance().selectResult("select * from znameni order by id_znameni", null);
			while (set.next()) {
				tmp.add(new Znameni(set.getString("nazev"), set.getInt("id_znameni"), set.getString("popis_efektu")));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return tmp;
	}

	@Override
	public Znameni getById(int id) {
		try {
			ResultSet set = Database.getInstance().selectResult("select * from znameni where id_znameni = ?",
					new QueryParam[] { new QueryParam<Integer>(id) });
			while (set.next()) {
				return new Znameni(set.getString("nazev"), set.getInt("id_znameni"), set.getString("popis_efektu"));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return null;
	}

	@Override
	public void save(Znameni entita) {
		try {
			if (entita.getId() != 0 && exists(entita)) {
				QueryParam<?>[] params = { new QueryParam<String>(entita.getNazev()),
						new QueryParam<String>(entita.getPopisEfektu()), new QueryParam<Integer>(entita.getId()) };
				Database.getInstance().save("update znameni set nazev = ?, popis_efektu = ? where id_znameni = ?",
						params);
			} else {
				QueryParam<?>[] params = { new QueryParam<String>(entita.getNazev()),
						new QueryParam<String>(entita.getPopisEfektu()) };
				Database.getInstance().save("insert into znameni(nazev, popis_efektu) values(?,?)", params);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	public void deleteById(int id) {
		try {
			Database.getInstance().delete(TablesEnum.ZNAMENI, id);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public Class<?> getClazz() {
		return Znameni.class;
	}

}
