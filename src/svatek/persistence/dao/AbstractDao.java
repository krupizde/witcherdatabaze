package svatek.persistence.dao;

import java.util.List;

import svatek.entity.Entita;

/**
 * 
 * @author Svatek
 * 
 *         Třída slouží jako předek pro všechny DAO objekty pro přístup do
 *         databáze. Třída opět přijímá generický datový typ, o kterém víme, že
 *         bude dědit od třídy Entita.
 * 
 * @param <T> Generický typ určující, se kterou třídou bude Dao pracovat
 */
public abstract class AbstractDao<T extends Entita> {

	/**
	 * Metoda vracející všechny instance T z databáze.
	 * 
	 * @return List všech entit typu T
	 */
	public abstract List<T> getAll();

	/**
	 * Metoda vrací jednu instanci typu T z databáze dle daného id
	 * 
	 * @param id Id dané instance
	 * @return
	 */
	public abstract T getById(int id);

	/**
	 * Metoda se pokusí vybrat z databáze záznam dle id, pro zjištění, zda takovýto
	 * záznam v databázi existuje
	 * 
	 * @param entita Objekt typu T, u kterého chceme zjistit, zda v databázi
	 *               existuje jeko záznam
	 * @return true pokud existuje, jinak false
	 */
	boolean exists(T entita) {
		return getById(entita.getId()) == null ? false : true;
	}

	/**
	 * Metoda uloží entitu do databáze, v implementaci nejprve zjistí, zda takový
	 * záznam již v databázi existuje a v závislosti na tom buď záznam vytvoří
	 * (insert) nebo aktualizuje (update)
	 * 
	 * @param entita Entita pro uložení
	 */
	public abstract void save(T entita);

	/**
	 * Metoda vymaže z databáze záznam dle daného id
	 * 
	 * @param id Id záznamu k vymazání
	 */
	public abstract void deleteById(int id);

	/**
	 * Metoda vrací Class objekt typu T (tedy v závislosti na T bude Class objekt
	 * jiný). Implementováno v potomcích
	 * 
	 * @return Class objekt typu T
	 */
	public abstract Class<?> getClazz();
}
