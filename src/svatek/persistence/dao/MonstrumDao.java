package svatek.persistence.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import svatek.entity.Monstrum;
import svatek.main.TablesEnum;
import svatek.persistence.Database;
import svatek.persistence.QueryParam;

public class MonstrumDao extends AbstractDao<Monstrum> {

	@Override
	public List<Monstrum> getAll() {
		ArrayList<Monstrum> tmp;
		try {
			tmp = new ArrayList<>();
			ResultSet set = Database.getInstance().selectResult("select * from monstrum order by id_monstrum", null);
			while (set.next()) {
				tmp.add(new Monstrum(set.getString("nazev"), set.getInt("id_monstrum"), set.getBoolean("boss"), set.getInt("id_kategorie")));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return tmp;
	}

	@Override
	public Monstrum getById(int id) {
		try {
			ResultSet set = Database.getInstance().selectResult("select * from monstrum where id_monstrum = ?",
					new QueryParam[] { new QueryParam<Integer>(id) });
			while (set.next()) {
				return new Monstrum(set.getString("nazev"), set.getInt("id_monstrum"), set.getBoolean("boss"), set.getInt("id_kategorie"));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return null;
	}

	@Override
	public void save(Monstrum entita) {
		try {
			if (entita.getId() != 0 && exists(entita)) {
				QueryParam<?>[] params = { new QueryParam<String>(entita.getNazev()),
						new QueryParam<Boolean>(entita.isBoss()), new QueryParam<Integer>(entita.getId()) };
				Database.getInstance().save("update monstrum set nazev = ?, boss = ? where id_monstrum = ?", params);
			} else {
				QueryParam<?>[] params = { new QueryParam<String>(entita.getNazev()),
						new QueryParam<Boolean>(entita.isBoss()) };
				Database.getInstance().save("insert into monstrum(nazev, boss) values(?,?)", params);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	public void deleteById(int id) {
		try {
			Database.getInstance().delete(TablesEnum.MONSTRUM, id);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public Class<?> getClazz() {
		return Monstrum.class;
	}

}
