package svatek.persistence;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import svatek.main.TablesEnum;

/**
 * 
 * @author Svatek
 * 
 *         Třída sloužící pro realizaci připojení k databázi. Slouží jako
 *         singleton se 'singleton' static parametrem Connection( Vytvoří se
 *         jednou při login(username,password)). Třída využívá Objekty typu
 *         QueryParam<T>.
 *
 */
public class Database {

	private Database() {

	}

	private static Database instance = null;
	private Connection connection = null;

	/**
	 * Singleton metoda pro tuto třídu
	 * 
	 * @return Poze jednu instanci Database
	 */
	public static Database getInstance() {
		if (instance == null)
			instance = new Database();
		return instance;
	}

	/**
	 * Metoda se pokusí vytvořit připojení s danou databází se zadanými
	 * přihlašovacími údaji. Pokud se to nepovede, metoda vyhodí vyjímku.
	 * 
	 * @param username Přihlašovací jméno
	 * @param password Přihlašovací heslo
	 */
	public void login(String username, String password) {
		try {
			Properties props = props();
			connection = DriverManager.getConnection("jdbc:mysql://" + props.getProperty("url") + ":"
					+ props.getProperty("port") + "/" + props.getProperty("database"), username, password);
			connection.setAutoCommit(true);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Getter pro connection
	 * 
	 * @return Připojení k databázi
	 */
	public Connection getConnection() {
		return connection;
	}

	/**
	 * Uzavře dané connection a odpojí se od databáze
	 */
	public void close() {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * Metoda slouží jako spouštěč pro zadané query. Vytvoří preparedStatement ze
	 * zadaného query, setne mu všechny parametry z pole 'params' a navrátí
	 * ResultSet výsledků. ? u QueryParams<?>[] říká, že nevíme jaké generické typy
	 * budou uvnitř objektů tohoto pole.
	 * 
	 * @param query  Query, které se má spustit
	 * @param params Pole parametrů pro dosazení do preparedStatementu
	 * @return ResultSet daného query
	 * @throws SQLException Při chybě při výběru
	 */
	public ResultSet selectResult(String query, QueryParam<?>[] params) throws SQLException {
		PreparedStatement stm = this.getConnection().prepareStatement(query);
		if (params != null) {
			for (int i = 0; i < params.length; i++) {
				addParam(stm, params[i], i + 1);
			}
		}
		return stm.executeQuery();

	}

	/**
	 * Přidá do PreparedStatementu parametr. Metoda je nezbytná, jelikož jdbc má pro
	 * nastavování různých typů parametrů různé metody, tedy v závislosti na datovém
	 * typu generického typu T z QueryParam se zavolá buď metoda setInt(),
	 * setString() nebo setBoolean(). Více v této aplikaci není potřeba
	 * 
	 * @param stm   PreparedStatement kterému se má parametr nastavi
	 * @param param Parametr k nastavení
	 * @param index Na jakou pozici v stm se má parametr nastavit
	 * @throws SQLException Při jakékoliv chybě
	 */
	protected <T> void addParam(PreparedStatement stm, QueryParam<T> param, int index) throws SQLException {
		if (param.getValue() instanceof Integer) {
			stm.setInt(index, (int) param.getValue());
			return;
		} else if (param.getValue() instanceof String) {
			stm.setString(index, (String) param.getValue());
			return;
		} else if (param.getValue() instanceof Boolean) {
			stm.setBoolean(index, (boolean) param.getValue());
			return;
		}

	}

	/**
	 * Metoda zavolá delete na danou tabulku s daným id. Metoda je citlivá na název
	 * tabulky a název sloupce id dané tabulky
	 * 
	 * @param tabulka Enum tabulky ve které se má mazat záznam
	 * @param id      Id záznamu ke smazání
	 * @throws SQLException Při jakékoliv chybě
	 */
	public void delete(TablesEnum tabulka, int id) throws SQLException {
		PreparedStatement stm = this.getConnection().prepareStatement(
				"delete from " + tabulka.name().toLowerCase() + " where id_" + tabulka.name().toLowerCase() + " = ?");
		stm.setInt(1, id);
		stm.executeUpdate();
	}

	/**
	 * Metoda vytvoří ze zadaného query preparedStatement a nastaví mu parametry
	 * podle pole params. Podobné jako u selectResult, pouze se místo stm.execute()
	 * zavola executeUpdate()
	 * 
	 * @param query  Query, ze kterého se vytvoří preparedStatement
	 * @param params Parametry pro query
	 * @throws SQLException Při jakékoliv chybě
	 */
	public <T> void save(String query, QueryParam<T>[] params) throws SQLException {
		if (params == null)
			throw new RuntimeException("Nelze vkládat nic");
		PreparedStatement stm = this.getConnection().prepareStatement(query);

		for (int i = 0; i < params.length; i++) {
			addParam(stm, params[i], i + 1);
		}
		stm.executeUpdate();
	}

	/**
	 * Načte soubor config.properties pro nastavení připojení
	 * 
	 * @return Properties s údaji pro připojení k databázi
	 */
	private Properties props() {
		Properties p = new Properties();
		try {
			p.load(new FileReader(new File("configuration/config.properties")));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return p;
	}

	/**
	 * Metoda se volá při zpracování jakékoliv RuntimeException vyjímky a zaloguje
	 * tuto vyjímku do databáze. Lze volat i při jiných logovacích událostech. Při
	 * vyhození vyjímky se nic nestane, jinak by se metoda zacyklila z vyhozené
	 * vyjímky.
	 * 
	 * @param name    Název události
	 * @param message Popis události
	 */
	public void addClientLog(String name, String message) {
		try {
			PreparedStatement stm = this.getConnection()
					.prepareStatement("insert into logclient(nazev, popis)values(?,?)");
			stm.setString(1, name);
			stm.setString(2, message);
			stm.executeUpdate();
		} catch (SQLException e) {
		}
	}
}
