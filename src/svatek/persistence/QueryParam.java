package svatek.persistence;

/**
 * 
 * @author Svatek
 * 
 *         Třída slouží pro realizaci Parametrů pro PreparedStatement, abych
 *         mohl do Database.selectResult posílat pole pouze jednoho datového
 *         typu a ne X různých parametrů. Generický typ T je v tomto případě buď
 *         String, Integer nebo Boolean. Objekty této třídy se využívají pouze v
 *         Dao a práce s nimi je ve třídě Database.
 *
 * @param <T> Datový typ parametru
 */
public class QueryParam<T> {

	private T value;

	public QueryParam(T value) {
		super();
		this.value = value;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

}
