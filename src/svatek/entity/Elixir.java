package svatek.entity;

import svatek.main.TablesEnum;

public class Elixir extends Entita{

	
	private String nazev;
	private String popisEfektu;
	
	public String getNazev() {
		return nazev;
	}
	public void setNazev(String nazev) {
		this.nazev = nazev;
	}
	public String getPopisEfektu() {
		return popisEfektu;
	}
	public void setPopisEfektu(String popisEfektu) {
		this.popisEfektu = popisEfektu;
	}
	public Elixir(int id, String nazev, String popisEfektu) {
		super();
		this.id = id;
		this.nazev = nazev;
		this.popisEfektu = popisEfektu;
	}
	
	public Elixir() {
	}
	@Override
	public TablesEnum getTabulka() {
		return TablesEnum.ELIXIR;
	}
	@Override
	public String[] getLine() {
		return new String[]{id+"", nazev, popisEfektu};
	}
	
	
	
}
