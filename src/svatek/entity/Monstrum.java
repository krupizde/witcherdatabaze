package svatek.entity;

import svatek.main.TablesEnum;

public class Monstrum extends Entita {

	private String nazev;
	private boolean boss;
	private int idKategorie;

	public Monstrum() {
		// TODO Auto-generated constructor stub
	}

	public Monstrum(String nazev, int id, boolean boss, int idKategorie) {
		super();
		this.id = id;
		this.nazev = nazev;
		this.boss = boss;
		this.idKategorie = idKategorie;
	}

	public String getNazev() {
		return nazev;
	}

	public void setNazev(String nazev) {
		this.nazev = nazev;
	}

	public boolean isBoss() {
		return boss;
	}

	public void setBoss(boolean boss) {
		this.boss = boss;
	}

	@Override
	public TablesEnum getTabulka() {
		return TablesEnum.MONSTRUM;
	}

	@Override
	public String[] getLine() {
		return new String[] { id + "", nazev, boss ? "Ano" : "Ne", idKategorie + "" };
	}
}
