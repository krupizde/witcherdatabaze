package svatek.entity;

import svatek.main.TablesEnum;

/**
 * 
 * @author Svatek
 * 
 *         Předek pro všechny Entity
 *
 */
public abstract class Entita {

	protected int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Metoda vrací enum odpovídající tabulce z databáze
	 * 
	 * @return Enum názvu tabulky
	 */
	public abstract TablesEnum getTabulka();

	/**
	 * Metoda která vrátí hodnoty všech proměnných jako Stringovské pole pro
	 * zobrazování v tabulkách
	 * 
	 * @return Hodnoty všech proměnných jako Stringovské pole
	 */
	public abstract String[] getLine();

}
