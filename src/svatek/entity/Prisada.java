package svatek.entity;

import svatek.main.TablesEnum;

public class Prisada extends Entita{

	private String nazev;
	
	public String getNazev() {
		return nazev;
	}
	public void setNazev(String nazev) {
		this.nazev = nazev;
	}
	public Prisada(int id, String nazev) {
		super();
		this.id = id;
		this.nazev = nazev;
	}
	public Prisada() {
	}
	@Override
	public TablesEnum getTabulka() {
		return TablesEnum.PRISADA;
	}
	@Override
	public String[] getLine() {
		return new String[]{id+"", nazev};
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nazev == null) ? 0 : nazev.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Prisada other = (Prisada) obj;
		if (nazev == null) {
			if (other.nazev != null)
				return false;
		} else if (!nazev.equals(other.nazev))
			return false;
		return true;
	}
	
	
}
