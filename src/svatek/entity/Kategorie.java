package svatek.entity;

import svatek.main.TablesEnum;

public class Kategorie extends Entita{
	
	private String nazev;
	private int idOlej;
	private int idZnameni;	
	public Kategorie(int id, int idOlej, int idZnameni, String nazev) {
		super();
		this.id = id;
		this.idOlej = idOlej;
		this.idZnameni = idZnameni;
		this.nazev = nazev;
	}
	public Kategorie() {
		// TODO Auto-generated constructor stub
	}
	public int getIdOlej() {
		return idOlej;
	}
	public void setIdOlej(int idOlej) {
		this.idOlej = idOlej;
	}
	public int getIdZnameni() {
		return idZnameni;
	}
	public void setIdZnameni(int idZnameni) {
		this.idZnameni = idZnameni;
	}
	public String getNazev() {
		return nazev;
	}
	public void setNazev(String nazev) {
		this.nazev = nazev;
	}
	@Override
	public TablesEnum getTabulka() {
		return TablesEnum.KATEGORIE;
	}
	@Override
	public String[] getLine() {
		return new String[]{id+"", nazev, idOlej+"", idZnameni+""};
	}
	
	
}
