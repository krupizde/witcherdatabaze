package svatek.entity;

import svatek.main.TablesEnum;

public class Znameni extends Entita {

	private String nazev;
	private String popisEfektu;

	public Znameni() {
	}

	public Znameni(String nazev, int id, String popisEfektu) {
		super();
		this.id = id;
		this.nazev = nazev;
		this.popisEfektu = popisEfektu;
	}

	public String getNazev() {
		return nazev;
	}

	public void setNazev(String nazev) {
		this.nazev = nazev;
	}

	public String getPopisEfektu() {
		return popisEfektu;
	}

	public void setPopisEfektu(String popisEfektu) {
		this.popisEfektu = popisEfektu;
	}

	@Override
	public TablesEnum getTabulka() {
		return TablesEnum.ZNAMENI;
	}

	@Override
	public String[] getLine() {
		return new String[]{id+"", nazev, popisEfektu};
	}

}
