# Praktická maturitní zkouška

**Střední průmyslová škola elektrotechnická, Praha 2, Ječná 30**
**Školní rok 2019/2020**
---
Jméno a příjimeni: Petr Svatek
Třída: ...
---

## Úvod
Zadání jsem se rozhodl řešit v databázovém systému MySql. Jako návrhové prostředí jsem využil 
Oracle DataModeler, z něhož jsem také vygeneroval, a adekvátně upravil, základní DDL. Pro administraci
a přístup k databázovému serveru jsem využil MySql Workbench. 
Pro tvorbu klienta jsem zvolil programovací jazyk Java s GUI knihovnou swing. Jako IDE jsem zvolil
Eclipse s nanstalovaným pluginem WindowBuilder pro grafickou tvorbu rozhraní.

## E-R model
Relační model databáze je dostupný ve složce Database/Relational_1.png jakožto obrázek, nebo ve složce
Database/Model jakožto Oracle DataModeler projekt. 

## Entitní integrita
Každá samostatná entita obsahuje automaticky generovaný primarní klíč typu INTEGER. 
Databázový model obsahuje dvě vazby M:N se slabou vazbou, primárním klíčem je tedy
kombinace dvou cizích klíčů ze samostatných entit, které jsou ovšem automaticky generované.

## Doménová integrita
** Znameni
- id_znameni - PK, číslo, automaticky generované
- nazev - libovolné znaky, not null, UNIQUE, délka 2 - 255 znaků
- popis_efektu - libovolné znaky, not null, délka 2 - 355 znaků

** Kategorie
- id_kategorie - PK, číslo, automaticky generované
- nazev - libovolné znaky, not null, UNIQUE, délka 2 - 255 znaků
- id_olej - FK, číslo, může být null
- id_znameni - FK, číslo, může být null

** Olej
- id_olej - PK, číslo, automaticky generované
- nazev - libovolné znaky, not null, UNIQUE, délka 2 - 255 znaků
- popis_efektu - libovolné znaky, not null, délka 2 - 355 znaků

** Elixir
- id_elixir - PK, číslo, automaticky generované
- nazev - libovolné znaky, not null, UNIQUE, délka 2 - 255 znaků
- popis_efektu - libovolné znaky, not null, délka 2 - 355 znaků

** Prisada
- id_prisada - PK, číslo, automaticky generované
- nazev - libovolné znaky, not null, UNIQUE, délka 2 - 255 znaků

** Monstrum
- id_monstrum - PK, číslo, automaticky generované
- nazev - libovolné znaky, not null, UNIQUE, délka 2 - 255 znaků
- id_kategorie - FK, číslo, může být null
- boss - boolean, not null

** logClient
- id_logClient - PK, číslo, automaticky generované
- nazev - libovolné znaky, not null, délka 1 - 100 znaků
- popis - libovolné znaky, délka 1 - 500 znaků

** logServer
- id_logServer - PK, číslo, automaticky generované
- nazev - libovolné znaky, not null, délka 1 - 300 znaků

** prisada_olej_rel
- id_prisada - číslo, not null, FK, PK
- id_olej - číslo, not null, FK, PK

** prisada_elixir_rel
- id_prisada - číslo, not null, FK, PK
- id_elixir - číslo, not null, FK, PK


## Referenční integrita
př:
** Návrh obsahuje několik cizích klíčů, které jsou uvedeny níže
- 'kategorie_olej_fk' ON DELETE set null
- 'kategorie_znameni_fk' ON DELETE set null
- 'monstrum_kategorie_fk' ON DELETE set null
- 'p_o_rel_olej_fk' ON DELETE cascade 
- 'p_o_rel_prisada_fkv1' ON DELETE cascade
- 'p_e_rel_elixir_fk' ON DELETE cascade
- 'p_e_rel_prisada_fkv1' ON DELETE cascade

## Indexy 
- Databáze má pro každou entitu pouze indexy vytvořené pro primární klíče, další indexy zatím databáze neobsahuje.

## Pohledy
- Návrh neobsahuje pohledy.

## Triggery
-	Databáze obsahuje triggery pro logování akcí Update a Delete na tabulkách Prisada, Olej, Elixir, Kategorie, Monstrum a Znameni. 
	Triggery se jmenují log_{TABULKA}_update/delete a vkládají záznamy do tabulky logServer před těmito akcemi.

## Uložené procedury a funkce
- Databáze obsahuje následující uložené procedury
* getPrisadyOleje(id_olej_param int) - vrátí přísady oleje s daným id
* getPrisadyElixiru(id_elixir_param int) - vrátí přísady elixiru s daným id
* getPocetMonsterKategorie(id_kategorie_param int) - vrátí počet monster z jednotlivých kategorií
* getPocetLogyClient() - vrátí počet logů sdílející stejný název (z klienta to např může být jméno Exception atp..)
* pridejPrisaduElixiru(id_elixir_param int, id_prisada_param int) - přidá elixiru s daným id přísadu s daným id
* pridejPrisaduOleji(id_olej_param int, id_prisada_param int) - přidá oleji s daným id přísadu s daným id
* odeberPrisaduElixiru(id_elixir_param int, id_prisada_param int) - odebere elixiru s daným id přísadu s daným id
* odeberPrisaduOleji(id_olej_param int, id_prisada_param int) - odebere oleji s daným id přísadu s daným id
- Databáze neobsahuje žádné uživatelské funkce

## Přístupové údaje do databáze

- 	Přístupové údaje ze zadávají v přihlašovacím okně při spuštění aplikace. Host, port a název databáze je nutno 
	nakonfigurovat v souboru confguration/confi.properties
pro vývoj byly použity tyto:
host		: localhost:3306
uživatel	: root
heslo		: heslo123
databáze	: witcherdatabaze

## Import struktury databáze a dat od zadavatele
Nejprve je nutné se připojit k databázovému serveru jako root uživatel s absolutními právy.
Poté je nutné spustit na serveru tyto scripty v tomto pořadí:
1 - Database/Script/Structure.sql
2 - Database/Script/Procedury.sql
3 - Database/Script/Users.sql
Script Structure.sql zároveň vytvoří samotné databázové schéma
Pokud si přejete nahrát i testovací data je nutné následně spustit script Database/Script/Data.sql

## Klientská aplikace
- Databáze obsahuje klientskou aplikaci napsanou v jazyce Java s uživatelským rozhraním
napsaným v knihovně Swing. Ovládání aplikace je intuitivní a nevyžaduje žádné speciální znalosti.

## Požadavky na spuštění
- Mysql server verze 5.6 nebo novější
- Mysql Workbench kompatibilní se verzí použitého mysql serveru, nebo jiné mysql vývojové prostředí
- připojení k internetu alespoň 2Mb/s
- Java JRE verze 1.8 nebo novější

## Návod na instalaci a ovládání aplikace

Uživatel by měl vytvořit databázi dle kroku "Import struktury databáze a dat od zadavatele".
Poté se přihlásit předdefinovaným uživatelem ze souboru Users.sql, nebo si vytvořit vlastního pomocí SQL příkazů
Měl by upravit konfigurační soubor klientské aplikace, aby odpovídal jeho podmínkám, tedy url, port a název databáze (obykle není třeba).
Spustit soubor app.jar dvojklikem, přihlásit se a může začít pracovat. V desktop aplikaci má každý uživatel dostupné všechny akce.
O bezpečnost a autorizaci se starají prává na straně databázového serveru. Pokud uživatel bude chtít mazat nějaká data
a nebude mít dostatečná práva, na straně aplikace bude mít tuto akci stále dostupnou, požadavek se odešle na databázový 
server, kde se vyhodnotí, že uživatel nemá oprávnění, v aplikace vyskočí vyjímka, která následně zobrazí message box
s danou zprávou. Stejné pro update/insert. Pokud uživatel nemá práva pro select, žádna vyjímka nevyskočí z designových důvodů
(při překlikávání radio buttonů pro výběr zdroje dat by pokaždé vyskočila vyjímka).

## Závěr
Databáze i aplikace jsou relativně v dobrém náběhu na skutečné využití v praxi. Největší vadou
je UX a UI klientské části, která by musela být vylepšena pro běžného uživatele.
