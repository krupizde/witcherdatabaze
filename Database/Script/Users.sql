start transaction;
CREATE USER 'selectUsers'@'%' IDENTIFIED BY 'heslo123';
GRANT SELECT ON witcherdatabase.* TO 'selectUsers'@'%';

CREATE USER 'almighty'@'%' IDENTIFIED BY 'vsemocny';
GRANT ALL PRIVILEGES ON witcherdatabase.* TO 'almighty'@'%';
commit;