start transaction;

-- UPDATE LOGY
create trigger log_elixir_update BEFORE UPDATE ON elixir
    FOR EACH ROW 
    insert into logserver
    set zprava = concat('update na tabulku elixir ', OLD.id_elixir);
    
create trigger log_olej_update BEFORE UPDATE ON olej
    FOR EACH ROW 
    insert into logserver
    set zprava = concat('update na tabulku olej ' , OLD.id_olej);
    
create trigger log_kategorie_update BEFORE UPDATE ON kategorie
    FOR EACH ROW 
    insert into logserver
    set zprava = concat('update na tabulku kategorie ' , OLD.id_kategorie);
    
create trigger log_monstrum_update BEFORE UPDATE ON monstrum
    FOR EACH ROW 
    insert into logserver
    set zprava = concat('update na tabulku monstrum ' , OLD.id_monstrum);
    
create trigger log_prisada_update BEFORE UPDATE ON prisada
    FOR EACH ROW 
    insert into logserver
    set zprava = concat('update na tabulku prisada ' , OLD.id_prisada);
    
create trigger log_znameni_update BEFORE UPDATE ON znameni
    FOR EACH ROW 
    insert into logserver
    set zprava = concat('update na tabulku znameni ', OLD.id_znameni);
    
    
-- DELETE LOGY
create trigger log_elixir_delete BEFORE delete ON elixir
    FOR EACH ROW 
    insert into logserver
    set zprava = concat('delete na tabulku elixir ' , OLD.id_elixir);
    
create trigger log_olej_delete BEFORE delete ON olej
    FOR EACH ROW 
    insert into logserver
    set zprava = concat('delete na tabulku olej ' , OLD.id_olej);
    
create trigger log_kategorie_delete BEFORE delete ON kategorie
    FOR EACH ROW 
    insert into logserver
    set zprava = concat('delete na tabulku kategorie ' , OLD.id_kategorie);
    
create trigger log_monstrum_delete BEFORE delete ON monstrum
    FOR EACH ROW 
    insert into logserver
    set zprava = concat('delete na tabulku monstrum ' , OLD.id_monstrum);
    
create trigger log_prisada_delete BEFORE delete ON prisada
    FOR EACH ROW 
    insert into logserver
    set zprava = concat('delete na tabulku prisada ' , OLD.id_prisada);
    
create trigger log_znameni_delete BEFORE delete ON znameni
    FOR EACH ROW 
    insert into logserver
    set zprava = concat('delete na tabulku znameni ' ,OLD.id_znameni);

-- DATA PROCEDURY

DELIMITER //
 
CREATE PROCEDURE getPrisadyOleje(id_olej_param int)
BEGIN
    SELECT prisada.id_prisada, prisada.nazev FROM prisada inner join prisada_olej_rel on prisada.id_prisada = prisada_olej_rel.id_prisada
    where prisada_olej_rel.id_olej = id_olej_param;
END //
 
DELIMITER ;

DELIMITER //
 
CREATE PROCEDURE getPrisadyElixiru(id_elixir_param int)
BEGIN
    SELECT prisada.id_prisada, prisada.nazev FROM prisada inner join prisada_elixir_rel on prisada.id_prisada = prisada_elixir_rel.id_prisada
    where prisada_elixir_rel.id_elixir = id_elixir_param;
END //
 
DELIMITER ;

DELIMITER //
 
CREATE PROCEDURE getPocetMonsterKategorie(id_kategorie_param int)
BEGIN
    SELECT count(id_monstrum) from monstrum where id_kategorie = id_kategorie_param;
END //
 
DELIMITER ;

DELIMITER //
 
CREATE PROCEDURE getPocetLogyClient()
BEGIN
	SELECT nazev, count(id_logClient) as pocet from logclient group by nazev order by count(id_logclient);
END //
 
DELIMITER ;

-- INSERT PROCEDURY

DELIMITER //
 
CREATE PROCEDURE pridejPrisaduElixiru(id_elixir_param int, id_prisada_param int)
BEGIN
	start transaction;
		insert into prisada_elixir_rel(id_elixir, id_prisada)values(id_elixir_param, id_prisada_param);
	commit;
END //
 
DELIMITER ;

DELIMITER //
 
CREATE PROCEDURE pridejPrisaduOleji(id_olej_param int, id_prisada_param int)
BEGIN
	start transaction;
		insert into prisada_olej_rel(id_olej, id_prisada)values(id_olej_param, id_prisada_param);
	commit;
END //
 
DELIMITER ;


-- DELETE PROCEDURY
DELIMITER //
 
CREATE PROCEDURE odeberPrisaduElixiru(id_elixir_param int, id_prisada_param int)
BEGIN
    start transaction;
        delete from prisada_elixir_rel where id_elixir = id_elixir_param and id_prisada = id_prisada_param;
    commit;
END //
 
DELIMITER ;

DELIMITER //
 
CREATE PROCEDURE odeberPrisaduOleji(id_olej_param int, id_prisada_param int)
BEGIN
    start transaction;
        delete from prisada_olej_rel where id_olej = id_olej_param and id_prisada = id_prisada_param;
    commit;
END //
 
DELIMITER ;

commit;