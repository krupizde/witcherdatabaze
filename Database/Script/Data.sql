start transaction;

insert into prisada(nazev)values('Unknown'),('Vlašotvičník'),('Calcium Vitae'),('Runová hvězda'),('Zpuchředlík'),('Krvavá houba'),('Zlobří srdce'),('Žabí srdce'),('Dwimeritový prášek'),('Ametystový prášek');

insert into elixir(nazev, popis_efektu)values('Vlaštovka','Regeneruje 150 životů v průběhu následujících 15 vteřin'),('Puštík', 'Regeneruje 150 energie v průběhu následujících 10 vteřin');

insert into prisada_elixir_rel(id_elixir, id_prisada)values(1,2),(1,4),(1,5),(2,3),(2,6);

insert into olej(nazev,popis_efektu)values('Olej proti zlobrovitým','Efekt trvá 30 seknutí, +20% poškození proti zlobrovitým'),('Olej proti přízrakům','Efekt trvá 35 seknutí, +25% poškození proti přízrakům');
insert into prisada_olej_rel(id_olej, id_prisada)values(1,7),(1,4),(1,9),(2,2),(2,10);
insert into kategorie(nazev)values('Unknown'),('Relikty'),('Prokletí');


insert into znameni(nazev, popis_efektu)values('Aard','Vyšle vlnu kinetické energie, která srazí nepřítele'),('Quen','Vytvoří štít kolem postavy trvající 1 útok'),('Igni','Vyšle vlnu ohně proti nepříteli');
insert into kategorie(nazev, id_olej, id_znameni)values('Zlobrovití',1, 1),('Přízraky',2,2);
insert into monstrum(id_kategorie, nazev, boss)values(5, 'Přízrak',0), (2, 'Golem',0), (3, 'Vlkodlak',0), (4, 'Kyklop',1);



    
commit;